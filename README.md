# Back-end Portfólio

Projeto de back-end com node, express e mongodb utilizado nas vídeo-aulas.

## Configuração do projeto
```
npm install
```

### Inicialização do servidor
```
npm run serve
```
