import mongoose from 'mongoose';

mongoose.connect('mongodb://localhost/portifio-pessoal', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
});
mongoose.Promise = global.Promise;

export default mongoose;
